import axios from "axios";

const taxrefApi = axios.create({
  baseURL: "https://taxref.mnhn.fr/api/taxa/",
  timeout: 1000
})

export {taxrefApi}