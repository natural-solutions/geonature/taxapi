import {
  GET,
  POST,
  JSONBODY,
  FILE,
  JSONRESPONSE,
  RequestLogger,
  root,
  PARAMETER,
} from "../tapis";
import express from "express";
import fs from "fs";
import { taxrefApi } from "../apiTaxref";

const taxonSch = {
  type: "object",
  properties: {
    REGNE: { type: "string" },
    PHYLUM: { type: "string" },
    CLASSE: { type: "string" },
    ORDRE: { type: "string" },
    FAMILLE: { type: "string" },
    SOUS_FAMILLE: { type: "string" },
    TRIBU: { type: "string" },
    GROUP1_INPN: { type: "string" },
    GROUP2_INPN: { type: "string" },
    CD_NOM: { type: "string" },
    CD_TAXSUP: { type: "string" },
    CD_SUP: { type: "string" },
    CD_REF: { type: "string" },
    RANG: { type: "string" },
    LB_NOM: { type: "string" },
    LB_AUTEUR: { type: "string" },
    NOM_COMPLET: { type: "string" },
    NOM_COMPLET_HTML: { type: "string" },
    NOM_VALIDE: { type: "string" },
    NOM_VERN: { type: "string" },
    NOM_VERN_ENG: { type: "string" },
    HABITAT: { type: "string" },
    FR: { type: "string" },
    GF: { type: "string" },
    MAR: { type: "string" },
    GUA: { type: "string" },
    SM: { type: "string" },
    SB: { type: "string" },
    SPM: { type: "string" },
    MAY: { type: "string" },
    EPA: { type: "string" },
    REU: { type: "string" },
    SA: { type: "string" },
    TA: { type: "string" },
    TAAF: { type: "string" },
    PF: { type: "string" },
    NC: { type: "string" },
    WF: { type: "string" },
    CLI: { type: "string" },
    URL: { type: "string" },
    description: { type: "string" },
    media: { type: "array", items: { type: "string" } },
  },
  required: [
    "REGNE",
    "PHYLUM",
    "CLASSE",
    "ORDRE",
    "FAMILLE",
    "SOUS_FAMILLE",
    "TRIBU",
    "GROUP1_INPN",
    "GROUP2_INPN",
    "CD_NOM",
    "CD_TAXSUP",
    "CD_SUP",
    "CD_REF",
    "RANG",
    "LB_NOM",
    "LB_AUTEUR",
    "NOM_COMPLET",
    "NOM_COMPLET_HTML",
    "NOM_VALIDE",
    "NOM_VERN",
    "NOM_VERN_ENG",
    "HABITAT",
    "FR",
    "GF",
    "MAR",
    "GUA",
    "SM",
    "SB",
    "SPM",
    "MAY",
    "EPA",
    "REU",
    "SA",
    "TA",
    "TAAF",
    "PF",
    "NC",
    "WF",
    "CLI",
    "URL",
  ],
};

const listTaxonSch = {
  type: "array",
  items: taxonSch,
};

root("taxapi/V1");

console.time("loadData");
const data = JSON.parse(fs.readFileSync("TAXREFv14.json", "utf-8"));
console.timeEnd("loadData");

class Ctrl {
  @GET("regnes")
  async regnes(req: express.Request, res: express.Response) {
    console.time("search");
    let result = [...new Set(data.map((e: any) => e["REGNE"]))];
    console.timeEnd("search");

    res.status(200).send(result);
  }

  @GET("phylums")
  async phylums(req: express.Request, res: express.Response) {
    console.time("search");
    let result = [...new Set(data.map((e: any) => e["PHYLUM"]))];
    console.timeEnd("search");

    res.status(200).send(result);
  }

  @GET("classes")
  async classes(req: express.Request, res: express.Response) {
    console.time("search");
    let result = [...new Set(data.map((e: any) => e["CLASSE"]))];
    console.timeEnd("search");

    res.status(200).send(result);
  }

  @GET("ordres")
  async ordres(req: express.Request, res: express.Response) {
    console.time("search");
    let result = [...new Set(data.map((e: any) => e["ORDRE"]))];
    console.timeEnd("search");

    res.status(200).send(result);
  }

  @GET("familles")
  async familles(req: express.Request, res: express.Response) {
    console.time("search");
    let result = [...new Set(data.map((e: any) => e["FAMILLE"]))];
    console.timeEnd("search");

    res.status(200).send(result);
  }

  @GET("groupe1")
  async groupe1(req: express.Request, res: express.Response) {
    console.time("search");
    let result = [...new Set(data.map((e: any) => e["GROUP1_INPN"]))];
    console.timeEnd("search");

    res.status(200).send(result);
  }

  @GET("groupe2")
  async groupe2(req: express.Request, res: express.Response) {
    console.time("search");
    let result = [...new Set(data.map((e: any) => e["GROUP2_INPN"]))];
    console.timeEnd("search");

    res.status(200).send(result);
  }

  @GET("taxons")
  @JSONRESPONSE("200", "Refresh token for new User", listTaxonSch)
  @PARAMETER({
    name: "NOM_COMPLET",
    schema: { type: "string" },
    in: "query",
    required: false,
  })
  @PARAMETER({
    name: "NOM_VERN",
    schema: { type: "string" },
    in: "query",
    required: false,
  })
  @PARAMETER({
    name: "REGNE",
    schema: { type: "string" },
    in: "query",
    required: false,
  })
  @PARAMETER({
    name: "CLASSE",
    schema: { type: "string" },
    in: "query",
    required: false,
  })
  @PARAMETER({
    name: "GROUP1_INPN",
    schema: { type: "string" },
    in: "query",
    required: false,
  })
  @PARAMETER({
    name: "GROUP2_INPN",
    schema: { type: "string" },
    in: "query",
    required: false,
  })
  @PARAMETER({
    name: "CD_NOM",
    schema: { type: "string" },
    in: "query",
    required: false,
  })
  @PARAMETER({
    name: "desc_media",
    schema: { type: "boolean" },
    in: "query",
    required: false,
  })
  async taxons(req: express.Request, res: express.Response) {
    console.time("search");
    let result = data.filter((e: any) => {
      let res: any = true;
      if (req.query["NOM_COMPLET"] && req.query["NOM_COMPLET"] !== "")
        res =
          res &
          e["NOM_COMPLET"]
            ?.toLowerCase()
            .includes(req.query["NOM_COMPLET"].toString().toLowerCase());
      if (req.query["NOM_VERN"] && req.query["NOM_VERN"] !== "")
        res =
          res &
          e["NOM_VERN"]
            ?.toLowerCase()
            .includes(req.query["NOM_VERN"].toString().toLowerCase());
      if (req.query["REGNE"] && req.query["REGNE"] !== "")
        res =
          res &
          e["REGNE"]
            ?.toLowerCase()
            .includes(req.query["REGNE"].toString().toLowerCase());
      if (req.query["CLASSE"] && req.query["CLASSE"] !== "")
        res =
          res &
          e["CLASSE"]
            ?.toLowerCase()
            .includes(req.query["CLASSE"].toString().toLowerCase());
      if (req.query["GROUP1_INPN"] && req.query["GROUP1_INPN"] !== "")
        res =
          res &
          e["GROUP1_INPN"]
            ?.toLowerCase()
            .includes(req.query["GROUP1_INPN"].toString().toLowerCase());
      if (req.query["GROUP2_INPN"] && req.query["GROUP2_INPN"] !== "")
        res =
          res &
          e["GROUP2_INPN"]
            ?.toLowerCase()
            .includes(req.query["GROUP2_INPN"].toString().toLowerCase());
      if (req.query["CD_NOM"] && req.query["CD_NOM"] !== "")
        res = res & e["CD_NOM"]?.toLowerCase().includes(req.query["CD_NOM"]);
      return res;
    });
    console.log(req.query);
    console.timeEnd("search");

    // console.time("recoverMedia");
    // if (req.query["desc_media"] && req.query["desc_media"] !== "false") {
    //   for (let e of result) {
    //     try {
    //       const { data } = await taxrefApi.get(`${e["CD_NOM"]}/media`);
    //       e.media = [];
    //       data["_embedded"]?.media.forEach((m: any) => {
    //         e.media.push(m["_links"].file.href);
    //       });
    //     } catch {}

    //     try {
    //       const res = await taxrefApi.get(`${e["CD_NOM"]}/factsheet`);
    //       e.description = res.data.text;
    //     } catch {}
    //   }
    // }
    // console.timeEnd("recoverMedia");

    res.status(200).send(result.slice(0, 25000));
  }
}

const ctrl = new Ctrl();

export default ctrl;
