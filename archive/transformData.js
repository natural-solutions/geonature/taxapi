const fs = require('fs')

console.time("readFile")
let data  = fs.readFileSync("TAXREFv14.txt", "utf-8")
console.timeEnd("readFile")

console.time("parseData")
data = data.split("\n")
const keys = data[0].replace("\r", "").split("\t")
data.shift()
data = data.map(e => {
  const values = e.replace("\r", "").split("\t")
  const taxon = {}
  for(i in keys){
    taxon[keys[i]] = values[i]
  }
  return taxon
})
console.timeEnd("parseData")


console.time("writeFile")
fs.writeFileSync("TAXREFv14.json", JSON.stringify(data))
console.timeEnd("writeFile")
